#include <Arduino.h>
#include <ISADefinitions.h>
#include <ISAButtons.h>
#include <SPI.h>
#include <SH1106_SPI.h>
#include <ISA7SegmentDisplay.h>
#include <DueTimer.h>
#include "Vector.h"

#undef BUTTONS
#define JOYSTICK

SH1106_SPI_FB lcd;
ISAButtons button;
ISA7SegmentDisplay seg;

int PONG_LEN = 10;

struct Position {
  Vector<int> x;
  Vector<int> y;
  int score = 0;
}player1, player2;

struct Ball{
  int x = 0;
  int y = 0;
}ball;

struct Direction
{
  int x = -1;
  int y = -1;
}direction;

struct Position *p1 = &player1;
struct Position *p2 = &player2;
struct Ball *f = &ball;
struct Direction *d = &direction;

// ------- START OF Ball -------

void set_random()
{
  int seed = 0;
  for(byte i=0; i<8; i++)
  {
    seed += analogRead(i);
  }
  randomSeed(seed);
}

void generate_Ball()
{
  f->x = 64;
  f->y = 32;
}

void draw_Ball()
{
  //lcd.setPixel(f->x, f->y, 0);
  generate_Ball();
  lcd.setPixel(f->x, f->y, 1);
  lcd.renderAll();
}

void clear_ball()
{
  lcd.setPixel(f->x, f->y, 0);
}

void draw_ball()
{
  lcd.setPixel(f->x, f->y, 1);
  lcd.renderAll();
}
// ------- END OF Ball -------



// ------- START OF NAVIGATION ------
void draw_position()
{
  for(byte i = 0; i < PONG_LEN; i++)
  {
    lcd.setPixel(p1->x[i], p1->y[i], 1);
    lcd.setPixel(p2->x[i], p2->y[i], 1);
  }
}

void clear_position()
{
  for(byte i = 0; i < PONG_LEN; i++)
  {
    lcd.setPixel(p1->x[i], p1->y[i], 0);
    lcd.setPixel(p2->x[i], p2->y[i], 0);
  }
}

void go_up()
{
  if(p1->y[PONG_LEN-1] - 1 <= 0)
  {
    return;
  }
  clear_position();
  for(byte i = 0; i < PONG_LEN; i++)
  {
      p1->y[i] -= 1;
  }
  draw_position();
  delayMicroseconds(20000);
}

void go_down()
{
  if(p1->y[0] + 1 >= 63)
  {
    return;
  }
  clear_position();
  for(byte i = 0; i < PONG_LEN; i++)
  {
      p1->y[i] += 1;
  }
  draw_position();
  delayMicroseconds(20000);
}

void go_up_2()
{
  if(p2->y[PONG_LEN-1] - 1 < 0)
  {
    return;
  }
  clear_position();
  for(byte i = 0; i < PONG_LEN; i++)
  {
      p2->y[i] -= 1;
  }
  draw_position();
  delayMicroseconds(20000);
}

void go_down_2()
{
  if(p2->y[0] + 1 >= 63)
  {
    return;
  }
  clear_position();
  for(byte i = 0; i < PONG_LEN; i++)
  {
      p2->y[i] += 1;
  }
  draw_position();
  delayMicroseconds(20000);
}

void p1_joy()
{
  // Serial.println(analogRead(JOY1X));
  // delayMicroseconds(20000);
  // analogInputToDigitalPin(p)
  if(analogRead(JOY1Y) > 600)
  {
    if(p1->y[PONG_LEN-1] - 1 <= 0)
    {
      return;
    }
    clear_position();
    for(byte i = 0; i < PONG_LEN; i++)
    {
        p1->y[i] -= 1;
    }
    draw_position();
    delayMicroseconds(20000);
  }
  if(analogRead(JOY1Y) < 400)
  {
    if(p1->y[0] + 1 >= 63)
    {
      return;
    }
    clear_position();
    for(byte i = 0; i < PONG_LEN; i++)
    {
        p1->y[i] += 1;
    }
    draw_position();
    delayMicroseconds(20000);
  }
}

void p2_joy()
{
  if(analogRead(JOY2Y) > 600)
  {
    if(p2->y[PONG_LEN-1] - 1 <= 0)
    {
      return;
    }
    clear_position();
    for(byte i = 0; i < PONG_LEN; i++)
    {
        p2->y[i] -= 1;
    }
    draw_position();
    delayMicroseconds(20000);
  }
  if(analogRead(JOY2Y) < 400)
  {
    if(p2->y[0] + 1 >= 63)
    {
      return;
    }
    clear_position();
    for(byte i = 0; i < PONG_LEN; i++)
    {
        p2->y[i] += 1;
    }
    draw_position();
    delayMicroseconds(20000);
  }
}
// ------- END OF NAVIGATION -------



// ------- START OF TIMER MOVE -------
void move()
{
  clear_ball();
  if(f->y - 1 <= 0)
  {
      d->y = 1;
  }
  if(f->y + 1 >= 63)
  {
    d->y = -1;
  }

  if(f->x -1 <=0)
  {
    d->x = 1;
  }
  if(f->x + 1 >= 128)
  {
    d->x = -1;
  }

  for(byte i = 0; i < PONG_LEN; i++)
  {
    if(p1->x[i] == f->x - 1 && p1->y[i] == f->y || p2->x[i] == f->x + 1 && p2->y[i] == f->y)
    {
      d->x = -d->x;
      d->y = -d->y;
    }
    if(f->x < p1->x[i] || f->x > p2->x[i])
    {
      if(d->x < 0)
      {
        p2->score++;
        seg.displayDigit(p2->score/10, 1);
        seg.displayDigit(p2->score%10, 0);
      }
      else
      {
        p1->score++;
        seg.displayDigit(p1->score/10, 3);
        seg.displayDigit(p1->score%10, 2, true);
      }
      f->x = 64;
      f->y = 32;
      return;
    }
  }

  f->x += d->x;
  f->y += d->y;

  draw_ball();
  //draw_Ball();
}
// ------- END OF TIMER MOVE -------


void setup()
{
  Serial.begin(9600);
  button.init();
  seg.init();
  for(byte i = 0; i < 4; i++)
  {
    seg.displayDigit(0, i);
  }
  pinMode(KEY_UP, INPUT);
  pinMode(KEY_DOWN, INPUT);
  pinMode(KEY_LEFT, INPUT);
  pinMode(KEY_RIGHT, INPUT);

  lcd.begin();

  for(byte i = 0; i < PONG_LEN; i++)
  {
    p1->y.PushBack(35-i);
    p1->x.PushBack(20);
    p2->y.PushBack(35-i);
    p2->x.PushBack(100);
    lcd.setPixel(p1->x[i], p1->y[i], 1);
    lcd.setPixel(p2->x[i], p2->y[i], 1);
    // lcd.print("TEST");
    // lcd.printNumber(1, 1);
  }

  for(byte i = 0; i < 128; i++)
  {
    lcd.setPixel(i, 0, 1);
    lcd.setPixel(i, 63, 1);
  }
  for(byte i = 0; i < 64; i++)
  {
    lcd.setPixel(0, i, 1);
    lcd.setPixel(127, i, 1);
  }

  set_random();
  draw_Ball();
  #ifdef BUTTONS
  attachInterrupt(KEY_UP, go_up, LOW);
  attachInterrupt(KEY_LEFT, go_down, LOW);
  attachInterrupt(KEY_RIGHT, go_up_2, LOW);
  attachInterrupt(KEY_DOWN, go_down_2, LOW);
  #endif
  Timer4.attachInterrupt(move);
  Timer4.setPeriod(50000);

  lcd.renderAll();
  Timer4.start();
}

void loop()
{
  #ifdef JOYSTICK
  p1_joy();
  p2_joy();
  #endif
}
